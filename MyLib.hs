module MyLib where

import Foreign.C.Types (CInt(..))

import Robotics.EV3 (BrickStatus(..), RobotHandle(..), BrickError(..))


foreign import ccall "ev3.h ev3_init" ev3Init :: IO CInt
foreign import ccall "ev3.h ev3_uninit" ev3Uninit :: IO ()

ev3Cycle :: IO ()
ev3Cycle = do
  _ <- ev3Init
  ev3Uninit


someFunc :: IO ()
someFunc = ev3Cycle


-- High level stuff here

hiEv3Init :: IO (Either BrickError RobotHandle)
hiEv3Init = do
  v <- ev3Init
  pure $ case toEnum $ fromIntegral v of
    Connected ->
      Right MkRobotHandle
    Disconnected ->
      Left BrickErrorFailedToInit

hiEv3Uninit :: IO (Either BrickError ())
hiEv3Uninit = undefined
