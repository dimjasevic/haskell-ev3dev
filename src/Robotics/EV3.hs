-- {-# LANGUAGE DataKinds          #-}
{-# LANGUAGE GADTs              #-}
{-# LANGUAGE StandaloneDeriving #-}

-- | Basics for working with EV3
module Robotics.EV3
  ( BrickStatus(..)
  , BrickError(..)
  , Robot(..)
  , RobotHandle(..)
  )
where

import           Data.Kind (Type)

-- data BrickStatus where
--   NotFound      :: BrickStatus
--   Found         :: BrickStatus
--   ErrorOccurred :: BrickStatus
data BrickStatus where
  Connected    :: BrickStatus
  Disconnected :: BrickStatus

deriving instance Eq BrickStatus

instance Enum BrickStatus where
  toEnum  0   = Disconnected
  toEnum  1   = Connected
  toEnum (-1) = Disconnected

  fromEnum Disconnected =  0
  fromEnum Connected    =  1

-- A dummy placeholder data type
data RobotHandle = MkRobotHandle

data Robot :: Type -> BrickStatus -> BrickStatus -> Type where
  Connect    :: Robot () 'Disconnected 'Connected
  Disconnect :: Robot () 'Connected 'Disconnected


data BrickError
  = BrickErrorFailedToInit
  | BrickErrorUninitFailed
  deriving (Eq, Show)
