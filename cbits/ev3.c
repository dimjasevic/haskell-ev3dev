#include <stdlib.h>
#include <time.h>


int ev3_init( void ) {
#ifdef DEBUG
  return 1;
#else //DEBUG
  // a random value from the set {-1, 0, 1} is returned with different
  // probabilities.
  srand(time(0));
  uint v = rand() % 100;
  if (v >= 90)
    return -1;
  else if (v >= 45)
    return 1;
  else
    return 0;
#endif //DEBUG
}


void ev3_uninit( void ) {
  // nothing to do here
}
