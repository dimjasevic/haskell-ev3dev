module Main where

import qualified MyLib

main :: IO ()
main = do
  putStrLn "Hello, Haskell!"
  v <- MyLib.hiEv3Init
  case v of
    Left _ -> putStrLn "Failed to initialise the robot!"
    Right _ -> putStrLn "Successfully initialised the robot!"
