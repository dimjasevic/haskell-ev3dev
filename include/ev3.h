#ifndef EV3_HASKELL
#define EV3_HASKELL

/**
 *  \brief Initialize the remote access and detect the EV3 brick.
 *  This function does nothing in the case of the local access.
 *  To disable the brick detection just specify IP address before the call of this function.
 *  \return 0 - the brick is NOT found or auto-detection is disabled; 1 - the brick is found;
 *  -1 - an error has occurred.
 */
extern int ev3_init( void );

/**
 *  \brief Uninitialize the remote access.
 */
extern void ev3_uninit( void );

#endif //EV3_HASKELL
