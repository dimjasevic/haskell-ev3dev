This project provides a Haskell library for interacting with ev3dev,
which covers the Lego  Mindstorms EV3 robots and others.

# Wrapping around the ev3dev C library

The implementation is a wrapper around the ev3dev C library:
https://github.com/in4lio/ev3dev-c.

# Contributing

One way to contribute is to provide a Haskell wrapper around a C
function that has not been wrapped yet. Please consider if it makes
sense to wrap a specific function: the goal is to provide at least the
capabilities of the C counterpart, and not to have a one-to-one
mapping between the C and Haskell libraries.


To introduce support for a C function that has not been wrapped yet,
consider the `source/ev3` directory in the `ev3dev-c`'s source code
repository. For example, there is `ev3.h` and one could be interested
in adding support for the `ev3_uninit` function.
